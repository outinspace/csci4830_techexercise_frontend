#!/bin/bash
set -f
echo "$SSH_PRIVATE_KEY" > key.pem
chmod 600 key.pem
mkdir ~/.ssh
ssh-keyscan $DEPLOY_SERVER >> ~/.ssh/known_hosts
mv target $APACHE_SUBDIR
scp -i key.pem -r $APACHE_SUBDIR ubuntu@$DEPLOY_SERVER:/var/www/html